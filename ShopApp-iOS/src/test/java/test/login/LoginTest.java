package test.login;

import org.testng.Assert;
import org.testng.annotations.Test;

import page.common.App;

import java.util.Arrays;
import java.util.List;

public class LoginTest extends App {

	/**
	 *  Scenario 1 - Login Tab
	 **/

	@Test(groups = { "Login", "TG-433", })
	public void checkLoginTabElements() {
		loginPage.tapLoginButton();
		Assert.assertNotNull(loginPage.checkLoginSubmitButton());

	}

	/**
	 * Scenario 2 - Login Tab
	 **/

	@Test(groups = { "Login", "TG-435", })
	public void checkLoginPageElements() {
		navigateToLoginScreen();
		Assert.assertNotNull(loginPage.checkCancelButton());
		Assert.assertNotNull(loginPage.checkloginText());
		Assert.assertNotNull(loginPage.checkPasswordTextFieldValue());
		Assert.assertNotNull(loginPage.checkloginStaticTextMessage());
		Assert.assertNotNull(loginPage.checkLoginSubmitButton());
		Assert.assertNotNull(loginPage.checkForgotPasswordButton());
	}

	private void navigateToLoginScreen() {

		loginPage.checkLoginButton();
		loginPage.tapLoginButton();
	}

	/**
	 *Scenario 3 - Valid Details Login OW-306 - Invalid
	 * Details Login
	 **/

	@Test(groups = { "Login", "TG-436", "TG-437" })
	public void verifyLoginFunctionality() {

		navigateToLoginScreen();
		// Enter invalid login credentials and verify the error message
		List<String> invalidList = Arrays.asList("emptyLogin", "pwdNull", "invalidEmail", "wrongPwd");
		for (String rootElement : invalidList) {
			loginPage.loginToApp(rootElement);
			loginPage.checkInvalidEmailErrorMessage();
			Assert.assertNotNull(loginPage.checkInvalidEmailErrorMessage());
			loginPage.tapcancelButton();

		}

		// Enter valid credentials and verify that user logged in successfully

		loginPage.loginToApp("normalUserInfo");
		

	}
}
