package auto.common.utils;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class AutoUtilities {
	
	

    /**
     * The static variable to hold test data from file as json object
     */
    public static JSONObject obj = null;

    /**
     * Method to load test data from a json file into json object "obj" once at the starting of test
     * suite ideally in @BeforeSuite method
     * 
     * @param fileName
     */
    public  static void loadTestData(String fileName) {
        JSONParser parser = new JSONParser();
        try {
        
            obj = (JSONObject) parser.parse(new FileReader(System.getProperty("user.dir") +fileName));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Method that takes any no of parameters in relation to the depth of json data structure to
     * read And return the value of the respective key. for example:
     * readTestData("userInfo","passion","code");
     * 
     * @param args
     * @return
     */
    public static String readTestData(String... args) {
        JSONObject temp = obj;
        if (args.length == 1) {
            return (String) obj.get(args[0]);
        } else {
            for (int i = 0; i < args.length - 1; i++) {
                temp = (JSONObject) temp.get(args[i]);
            }
            return (String) temp.get(args[args.length - 1]);
        }
    }

}
