package auto.common;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import com.google.common.base.Function;

import io.appium.java_client.AppiumDriver;

public class AppiumWrapper extends InitAppium {

	
	public static WebElement find(final By locator, int... args) {

		int timeout = (args.length > 0 ? args[0] : 15);
		WebElement webelement = null;

		try {
			FluentWait<AppiumDriver> wait = new FluentWait<AppiumDriver>(driver).withTimeout(timeout, TimeUnit.SECONDS)
					.pollingEvery(200, TimeUnit.MILLISECONDS).ignoring(Exception.class)
					.ignoring(NoSuchElementException.class);

			webelement = wait.until(new Function<AppiumDriver, WebElement>() {
				public WebElement apply(AppiumDriver driver) {
					return driver.findElement(locator);
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return webelement;

	}

	
	public void tapElement(By locator) {
		WebElement element = find(locator);
		element.click();
	}

	public static void typeValue(String inputValue, By locator) {

		find(locator).sendKeys(inputValue);

	}
	public static void startApp() {
		driver.launchApp();

	}

	
	public static void closeApp() {
		driver.closeApp();
	}

	
	public static void resetApp() {
		driver.resetApp();
	}


	
	public void tapElement(WebElement element) {

		element.click();
	}	

}
