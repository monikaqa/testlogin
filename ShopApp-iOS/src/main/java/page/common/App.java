package page.common;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import auto.common.InitAppium;
import auto.common.utils.AutoUtilities;
import page.login.LoginPage;

public class App extends InitAppium {
	
	@BeforeMethod(alwaysRun = true)
	public void beforeEachTest() throws Exception {
	
		System.out.println("Start App called ");
		
		// Laucnh the app
		driver.launchApp();	
		
	}
	public LoginPage loginPage = null;
	public AutoUtilities utils = null;
	
	public App() {
			// Initializing the various pages of the app for use across all tests
		loginPage = new LoginPage();
	}
	
	@AfterMethod(alwaysRun = true)
	public void afterEachTest() throws Exception {
		try {
			driver.closeApp();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

    
	
}
