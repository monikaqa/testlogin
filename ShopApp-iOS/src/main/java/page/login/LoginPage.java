package page.login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import auto.common.AppiumWrapper;

public class LoginPage extends AppiumWrapper {

	private By registerButton = By.id("Register");
	private By cancelButton = By.id("Cancel");
	private By loginText = By.xpath("//XCUIElementTypeStaticText[@name='Log in']");
	private By loginTitle = By.xpath("//XCUIElementTypeNavigationBar[@name='Log in']");
	private By loginTextFieldValue = By.xpath("//XCUIElementTypeTextField[@value='Email or User ID']");

	private By passwordTextFieldValue = By.xpath("//XCUIElementTypeSecureTextField[@value='Password']");
	private By loginStaticTextMessage = By.xpath(
			"//XCUIElementTypeStaticText[@value='To log in, use your email address and password. For older accounts, use your Log In ID instead of your email address.']");
	private By loginButton = By.id("AccountViewController.notLoggedInHeader.loginButton");
	private By logInSubmitButton = By.xpath("//XCUIElementTypeButton[@name='Log In']");
	private By ForgotPasswordButton = By.id("Forgot Password?");
	private By invalidEmailErrorMessage = By.id("Invalid Email, Log In ID or Password");
	private By reviewCartFrameTitle = By.id("Your previous cart items were added to continue where you last left off.");
	private By reviewCartButton = By.id("Review");
	private By noThanksButton = By.id("No Thanks");
	private By managemyaccountbutton = By.id("Manage My Account");
	private By noThanksLoginButton = By.id("No Thanks");
	private By rememberLoginbutton = By.id("Remember");

	private By listLoginButton = By.id("ListsViewController.listFooterView.loginButton");
	

	public void tapLoginButton() {
		tapElement(loginButton);
	}

	public void tapListLoginButton() {
		tapElement(listLoginButton);
	}

	public void tapLoginSubmitButton() {
		tapElement(logInSubmitButton);
	}

	public void tapNoThanksLoginButton() {
		tapElement(noThanksLoginButton);
	}

	public WebElement checkNoThanksLoginButton() {
		return find(noThanksLoginButton);
	}

	public void tapRememberLoginbutton() {
		tapElement(rememberLoginbutton);
	}

	public void tapLogInSubmitButton() {
		tapElement(logInSubmitButton);
	}

	public void loginToApp(String emailValue, String pwdValue) {
		typeValue(emailValue, loginTextFieldValue);
		// loginPage.tapPasswordField();
		typeValue(pwdValue, passwordTextFieldValue);
		// Tap login button
		tapLogInSubmitButton();
	}

	public void loginToApp(String UserAccount, boolean... args) {

		boolean clickReviewCart = (args.length > 0 ? false : true);

		// Tap on Login button
		if (checkLoginSubmitButton() == null){
			tapLoginButton();
		}
		String username, password;
		// Read Username from Json File
		username = util.readTestData(UserAccount, "userName");
		password = util.readTestData(UserAccount, "password");

		// Fill username and password
		typeValue(username, loginTextFieldValue);
		typeValue(password, passwordTextFieldValue);

		// Tap Login button
		tapLogInSubmitButton();

		if (checkNoThanksLoginButton() != null) {
			tapNoThanksLoginButton();
		}

		// Verify Review Cart Frame and Tap on the "No Thanks button in the
		// Popped up Frame"
		if (clickReviewCart) {
			if (find(reviewCartButton) != null)
				tapNoThanksButton();
		}

	}

	
	public void tapLoginTextFieldValue() {
		tapElement(loginTextFieldValue);
	}

	public void enterEmailId(String email) {
		typeValue(email, loginTextFieldValue);
	}

	public void tapPasswordTextFieldValue()

	{
		tapElement(passwordTextFieldValue);
	}

	public void enterPassword(String password) {
		typeValue(password, passwordTextFieldValue);
	}

	public void tapForgotPasswordButton() {
		tapElement(ForgotPasswordButton);
	}

	public void tapRegisterButton() {
		tapElement(registerButton);
	}

	public void tapcancelButton() {
		tapElement(cancelButton);
	}

	public void tapReviewCartButton() {
		tapElement(reviewCartButton);
	}

	public void tapNoThanksButton() {
		tapElement(noThanksButton);
	}

	public WebElement checkLoginSubmitButton() {
		return find(logInSubmitButton);
	}

	public WebElement checkLoginButton() {
		return find(loginButton, 15);
	}

	public WebElement checkListLoginButton() {
		return find(listLoginButton, 15);
	}

	public WebElement checkRegisterButton() {
		return find(registerButton);
	}

	public WebElement checkCancelButton() {
		return find(cancelButton, 25);
	}

	public WebElement checkloginButton() {
		return find(loginButton);
	}

	public WebElement checkloginText() {

		return find(loginText);

	}

	public WebElement checkloginTitle() {
		return find(loginTitle);
	}

	public WebElement checkPasswordTextFieldValue() {
		return find(passwordTextFieldValue);
	}

	public WebElement checkloginStaticTextMessage() {
		return find(loginStaticTextMessage);
	}

	public WebElement checkForgotPasswordButton() {
		return find(ForgotPasswordButton);
	}

	public WebElement checkInvalidEmailErrorMessage() {
		return find(invalidEmailErrorMessage);
	}

	public WebElement checknoThanksButton() {
		return find(noThanksButton);
	}

	public WebElement checkreviewCartFrameTitle() {
		return find(reviewCartFrameTitle);
	}

	public WebElement checkreviewCartButton() {
		return find(reviewCartButton);
	}

	public WebElement Managemyaccountbutton() {
		return find(managemyaccountbutton);
	}

}
